import java.util.ArrayList;

public class Helper
{
    public void arrayl()
    {
        ArrayList<String> mylist = new ArrayList<String>();


        //add array elements

        String a = "i";
        mylist.add(a);
        String b = "am";
        mylist.add(b);
        String c = "yogesh";
        mylist.add(c);
        String d = "hello";
        mylist.add(d);

        System.out.println("array element is  :" + mylist);

        //remove element in array
       // mylist.remove(3);
       // mylist.remove("i");
        //mylist.remove("am");
        //mylist.remove("yogesh");
         mylist.remove("hello");
        System.out.println("array element is  :" + mylist);

         //check elemnt is present or not
        System.out.println("It is present :" + mylist.contains("yogesh"));

        //check for array is empty or not
        System.out.println("array is empty :" + mylist.isEmpty());

        //check index of any element(searching)
        System.out.println("index of the element is :" +mylist.indexOf("yogesh"));

        //get the total no of elements in array(array size)
        System.out.println("size of array is  :" +mylist.size());

        //check the element at any index
        System.out.println("element at this index is :" + mylist.get(2));




        System.out.println("array element is  :" + mylist);

    }
}
